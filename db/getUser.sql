-- When passing an array to this SQL you can use the position with a $ (ex. $1) and it'll pass that variable.
-- In this case $1 is the user id.
-- It start with position 1 not 0 because it's weird.
SELECT * FROM users WHERE ID = $1

