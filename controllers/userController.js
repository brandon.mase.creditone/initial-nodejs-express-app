module.exports = {
    /**
     * Gets the name of the user.
     * @param {*} request - This is the request that comes in from the api call.
     * @param {*} response - This is the response that you will send. So you can add whatever you want then that gets sent back as a response.
     */
    getName: (request, response) => {

        //* The massive POSTGRES server also gets passed in the request.
        //* This grabs the server from the request.
        const db = request.app.get('db');

        //* Get the userID parameter from the request.
        const userID = request.params.userID;

        //* call the getUser SQL statement from the ../db/getUser
        //* If you pass an array of variables to the SQL statement you can use the array element position in the SQL statement.
        //* see the ../db/getUser.sql file for more info.
        db.getUser([userID])
            //* the .then method is what happens after the DB call is finished and passed back.
            .then((dbResponse) => {
                //* Get the first and last name from the response of the SQL statement
                const {
                    firstName,
                    lastName
                } = dbResponse;

                //* Send the first and last name as the response with the a status of 200.
                response.status(200).json({
                    firstName: firstName,
                    lastName: lastName
                })
            })
            //* If the SQL statement fails it come into this function
            .catch((error) => {
                //* Send the error as the response with a status of 500.
                response.status(500).json(error)
            })
    }
}