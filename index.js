//* Actual server is express.
const express = require('express');

//* This automatically converts the request response return to JSON instead of having to parse it manually.
const bodyParser = require('body-parser');

//* HTTP Requests
const axios = require('axios');

//* Be able to use the .env file variables.
//* This is so you don't have to upload your API keys to a repo. You would just add the file to Openshift
//* Any variable in the .env file would be used like process.env.<YOUR VARIABLE NAME>
require('dotenv').config();

//* This is what we use to connect to a POSTGRES db. If we are using anything else we just need to find a package that integrates into it.
const massive = require('massive');

//* Connect to the POSTGRES db.
//* Connect with the CONNECTION_STRING variable from the .env file.
//* .env files won't be uploaded to gitlab
//* Create a .env file in the root and add a CONNECTION_STRING = <YOUR CONNECTION STRING> to be able to use this function.
// massive(process.env.CONNECTION_STRING).then(db => app.set('db', db)).catch(e => console.log("massive error", e));
const app = express();


app.use(bodyParser.json());


//* Import the controller from the ./controllers folder.
const userController = require('./controllers/userController');

//* This is the route for a GET request to /api/getName/:userID
//* :userID is the userID passed as a parameter to this api.
//* example: /api/getName/12334 <- This is the request url and it will pass 12334 as the userID.
//* After the URL is hit, it will go to the userController.getName callback function.
//* See ./controllers/userController for information on what to do with the request/response.  
app.get('/api/getName/:userID', userController.getName);

//* A POST request to /api/createUser. This will contain a body also
//* Instead of getting the parameters, you would get the body from the response
//* Would use const body = request.body instead of request.params
app.post('/api/createUser/', userController.getName);


//* A GET request to /api/getComment?id=:commentID. 
//* :commentID is the comment id passed a query to this API.
//* example: /api/getComment?id=230984 <- This is the request url and it will pass 230984 as the userID.
//* In the controller you get the query as const commentID = request.query.commentID
app.get('/api/getComment?id=:commentID', userController.getName);


//* The port number to run the server;
const PORT = 4000;

//* This starts the server at localhost:4000
app.listen(PORT, () => console.log(`Listening on PORT ${PORT}`));