# After you get node, git and npm installed


Open up the root directory in git and run `npm install`.

After npm install is done, to run the server run `nodemon start`.

`nodemon` will watch for changes in the files and restart the server on change.

If all goes well you should see "Listening on PORT 4000" in the console.